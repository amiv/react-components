function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var PropTypes = _interopDefault(require('prop-types'));
var Ajv = _interopDefault(require('ajv-draft-04'));
var material = require('@mui/material');
var mui = require('tss-react/mui');
var LaunchIcon = _interopDefault(require('@mui/icons-material/Launch'));
var createSvgIcon = _interopDefault(require('@mui/icons-material/utils/createSvgIcon'));
var HelpOutlineIcon = _interopDefault(require('@mui/icons-material/HelpOutline'));
var Slide = _interopDefault(require('@mui/material/Slide'));
var ErrorOutlineIcon = _interopDefault(require('@mui/icons-material/ErrorOutline'));
var Paper = _interopDefault(require('@mui/material/Paper'));
var InputBase = _interopDefault(require('@mui/material/InputBase'));
var IconButton = _interopDefault(require('@mui/material/IconButton'));
var ClearIcon = _interopDefault(require('@mui/icons-material/Clear'));
var SearchIcon = _interopDefault(require('@mui/icons-material/Search'));
var CloseIcon = _interopDefault(require('@mui/icons-material/Close'));

var isBrowser = function isBrowser() {
  return typeof window !== "undefined";
};
var isObject = function isObject(value) {
  return typeof value === 'object' && value !== null;
};
var isDevEnvironment = function isDevEnvironment() {
  return process.env.NODE_ENV !== 'production' && !process.env.REACT_PRODUCTION_DEBUGGING;
};

function log(message) {
  if (isDevEnvironment) console.log(message);
}
function warning(message) {
  if (isDevEnvironment) console.warn(message);
}
function error(message) {
  if (isDevEnvironment) console.error(message);
}

var Query = /*#__PURE__*/function () {
  function Query() {}
  Query.isEqual = function isEqual(query1, query2) {
    return JSON.stringify(query1) === JSON.stringify(query2);
  };
  Query.copy = function copy(query) {
    return JSON.parse(JSON.stringify(query || {}));
  };
  Query.merge = function merge() {
    var newQuery = {};
    var queryFunctions = [];
    for (var _len = arguments.length, queries = new Array(_len), _key = 0; _key < _len; _key++) {
      queries[_key] = arguments[_key];
    }
    queries.forEach(function (query) {
      if (typeof query === 'function') {
        queryFunctions.push(query);
        return;
      }
      Object.entries(query).forEach(function (_ref) {
        var key = _ref[0],
          value = _ref[1];
        if (newQuery[key]) {
          if (key === '$or') {
            if (!newQuery.$and) newQuery.$and = [];
            newQuery.$and.push({
              $or: Query.copy(value)
            });
          } else if (key === '$and') {
            newQuery.$and = newQuery.$and.concat(Query.copy(value));
          } else if (typeof newQuery[key] === 'object') {
            newQuery[key] = Query.merge(newQuery[key], value);
          } else {
            newQuery[key] = value;
          }
        } else {
          newQuery[key] = Query.copy(value);
        }
      });
    });
    if (queryFunctions.length === 0) return newQuery;
    return function () {
      return Query.merge.apply(Query, [newQuery].concat(queryFunctions.map(function (queryFunction) {
        return queryFunction();
      })));
    };
  };
  Query.buildQueryString = function buildQueryString(query) {
    var parsedQuery = new URLSearchParams();
    Object.keys(query).forEach(function (key) {
      var value = null;
      if (Array.isArray(query[key])) {
        if (key === 'sort') {
          value = query[key].join(',');
        } else {
          value = query[key];
        }
      } else {
        value = JSON.stringify(query[key]);
      }
      parsedQuery.set(key, value);
    });
    return parsedQuery.toString();
  };
  return Query;
}();

var sleep = function sleep(milliseconds) {
  return new Promise(function (resolve) {
    setTimeout(resolve, milliseconds);
  });
};

function _extends() {
  _extends = Object.assign ? Object.assign.bind() : function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  return _extends.apply(this, arguments);
}
function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;
  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }
  return target;
}

var FormContext = React__default.createContext(null);
function JsonSchemaForm(_ref) {
  var schema = _ref.schema,
    onChange = _ref.onChange,
    renderLabel = _ref.renderLabel,
    enumSelectThreshold = _ref.enumSelectThreshold,
    initialValues = _ref.initialValues,
    initialValid = _ref.initialValid,
    children = _ref.children;
  var _useState = React.useState(null),
    currentSchema = _useState[0],
    setCurrentSchema = _useState[1];
  var _useState2 = React.useState(null),
    ajv = _useState2[0],
    setAjv = _useState2[1];
  var _useState3 = React.useState({}),
    errors = _useState3[0],
    setErrors = _useState3[1];
  var _useState4 = React.useState(initialValues),
    values = _useState4[0],
    setValues = _useState4[1];
  var _useState5 = React.useState(initialValid),
    isValid = _useState5[0],
    setIsValid = _useState5[1];
  var validate = React.useCallback(function (newValues) {
    if (!ajv) {
      setErrors({});
      return true;
    }
    var ajvValidate = ajv.getSchema('schema');
    if (ajvValidate) {
      var newIsValid = ajvValidate(newValues);
      setIsValid(newIsValid);
      if (newIsValid) {
        setErrors({});
      } else {
        var newErrors = {};
        ajvValidate.errors.forEach(function (error) {
          var field = error.instancePath.substring(1);
          if (newErrors[field]) {
            newErrors[field].push(error.message);
          } else {
            newErrors[field] = [error.message];
          }
        });
        setErrors(newErrors);
      }
      return newIsValid;
    }
    return initialValid;
  }, [setErrors, setIsValid, ajv]);
  var changeHandler = React.useCallback(function (_ref2) {
    var _extends2;
    var name = _ref2.name,
      value = _ref2.value;
    var newValues = _extends({}, values, (_extends2 = {}, _extends2[name] = value, _extends2));
    setValues(newValues);
    var newIsValid = validate(newValues);
    onChange({
      values: newValues,
      isValid: newIsValid
    });
  }, [onChange, setValues, validate]);
  var getErrorsForField = React.useCallback(function (fieldName) {
    return errors[fieldName] || [];
  }, [errors]);
  React.useEffect(function () {
    var newValues = _extends({}, initialValues, values);
    setValues(newValues);
    var newIsValid = validate(newValues);
    onChange({
      values: newValues,
      isValid: newIsValid
    });
  }, [initialValues]);
  React.useEffect(function () {
    var modifiedSchema = function modifiedSchema() {
      var objectSchema = _extends({}, schema);
      Object.keys(objectSchema.properties).forEach(function (property) {
        if (objectSchema.properties[property].type === 'media' || objectSchema.properties[property].type === 'json_schema_object') {
          objectSchema.properties[property].type = 'object';
        }
        if (objectSchema.properties[property].format === 'objectid') {
          delete objectSchema.properties[property];
        } else if (objectSchema.properties[property].nullable) {
          objectSchema.properties[property].type = ['null', objectSchema.properties[property].type];
          if ('enum' in objectSchema.properties[property]) {
            objectSchema.properties[property]["enum"].push(null);
          }
        }
      });
      return objectSchema;
    };
    if (currentSchema === JSON.stringify(schema)) return;
    if (ajv) {
      warning('JsonSchemaForm: It is not recommended to change the schema of a form during its use!');
    }
    if (!schema) {
      setAjv(null);
      setErrors({});
      setCurrentSchema(null);
      warning('JsonSchemaForm: It is not recommended to use without a schema!');
      return;
    }
    var newAjv = new Ajv({
      schemaId: 'auto',
      missingRefs: 'ignore',
      errorDataPath: 'property',
      allErrors: true
    });
    newAjv.addSchema(modifiedSchema(), 'schema');
    setAjv(newAjv);
    setErrors({});
    setCurrentSchema(JSON.stringify(schema));
  }, [schema]);
  var contextValue = React.useMemo(function () {
    return {
      schema: schema,
      enumSelectThreshold: enumSelectThreshold,
      isValid: isValid,
      values: values,
      onChange: changeHandler,
      renderLabel: renderLabel,
      getErrorsForField: getErrorsForField
    };
  }, [schema, enumSelectThreshold, isValid, values, changeHandler, renderLabel, getErrorsForField]);
  return /*#__PURE__*/React__default.createElement(FormContext.Provider, {
    value: contextValue
  }, children);
}
JsonSchemaForm.propTypes = {
  schema: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  enumSelectThreshold: PropTypes.number,
  initialValues: PropTypes.object,
  initialValid: PropTypes.bool,
  children: PropTypes.node.isRequired,
  renderLabel: PropTypes.func
};
JsonSchemaForm.defaultProps = {
  enumSelectThreshold: 4,
  initialValues: {},
  initialValid: true,
  onChange: function onChange() {},
  renderLabel: function renderLabel(label) {
    return label;
  }
};
var useForm = function useForm() {
  return React__default.useContext(FormContext);
};

var _excluded = ["name", "label", "description", "disabled"];
function JsonSchemaFormField(_ref) {
  var name = _ref.name,
    labelProp = _ref.label,
    descriptionProp = _ref.description,
    disabled = _ref.disabled,
    props = _objectWithoutPropertiesLoose(_ref, _excluded);
  var _useForm = useForm(),
    schema = _useForm.schema,
    enumSelectThreshold = _useForm.enumSelectThreshold,
    values = _useForm.values,
    onChange = _useForm.onChange,
    renderLabel = _useForm.renderLabel,
    getErrorsForField = _useForm.getErrorsForField;
  if (!schema || !schema.properties || !schema.properties[name]) {
    warning("FormField: property \"" + name + "\" not found in FormContext!");
    return null;
  }
  var field = schema.properties[name];
  var errors = getErrorsForField(name);
  var hasErrors = errors.length > 0;
  var value = values[name];
  var handleChange = function handleChange(e) {
    onChange({
      name: name,
      value: e.target.value
    });
  };
  var label = renderLabel(labelProp || field.label || field.title || name);
  var description = descriptionProp || field.description;
  var help = hasErrors ? errors.join(', ') : description;
  var type = field.type;
  if (Array.isArray(field.type)) {
    if (field.type[0] === 'null') {
      var _field$type = field.type;
      type = _field$type[1];
    } else {
      var _field$type2 = field.type;
      type = _field$type2[0];
    }
  }
  if (field["enum"]) {
    if (field["enum"].length < enumSelectThreshold) {
      return /*#__PURE__*/React__default.createElement(material.RadioGroup, _extends({
        name: name,
        value: value,
        onChange: handleChange
      }, props), field["enum"].map(function (item) {
        return /*#__PURE__*/React__default.createElement(material.FormControlLabel, {
          key: item,
          value: item,
          disabled: disabled,
          control: /*#__PURE__*/React__default.createElement(material.Radio, null),
          label: label
        });
      }));
    }
    return /*#__PURE__*/React__default.createElement(material.FormControl, _extends({
      variant: "outlined"
    }, props), /*#__PURE__*/React__default.createElement(material.InputLabel, null, label), /*#__PURE__*/React__default.createElement(material.Select, {
      variant: "outlined",
      "native": true,
      name: name,
      value: value || '',
      label: label,
      disabled: disabled,
      onChange: handleChange
    }, /*#__PURE__*/React__default.createElement("option", {
      key: "null",
      value: null,
      disabled: true,
      hidden: true
    }), field["enum"].map(function (item) {
      return /*#__PURE__*/React__default.createElement("option", {
        key: item,
        value: item
      }, renderLabel(item));
    })));
  }
  if (type === 'string') {
    if (field.format === 'date-time') {
      return /*#__PURE__*/React__default.createElement(material.TextField, _extends({
        name: name,
        label: label,
        helperText: help,
        error: hasErrors,
        disabled: disabled,
        type: "datetime-local",
        value: value || '',
        onChange: handleChange
      }, props));
    }
    return /*#__PURE__*/React__default.createElement(material.TextField, _extends({
      name: name,
      label: label,
      helperText: help,
      error: hasErrors,
      disabled: disabled,
      value: value || '',
      type: "text",
      onChange: handleChange
    }, props));
  }
  if (type === 'number' || type === 'integer') {
    return /*#__PURE__*/React__default.createElement(material.TextField, _extends({
      name: name,
      label: label,
      helperText: help,
      error: hasErrors,
      disabled: disabled,
      value: value || 0,
      type: "number",
      onChange: handleChange
    }, props));
  }
  if (type === 'boolean') {
    return /*#__PURE__*/React__default.createElement(material.Checkbox, {
      name: name,
      checked: value,
      disabled: disabled,
      onChange: function onChange(e) {
        return handleChange(_extends({}, e, {
          target: _extends({}, e.target, {
            value: e.target.checked
          })
        }));
      }
    });
  }
  warning("JsonSchemaForm: field type '" + type + "' with name '" + name + "' not supported!");
  return null;
}
JsonSchemaFormField.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  description: PropTypes.string,
  disabled: PropTypes.bool
};
JsonSchemaFormField.defaultProps = {
  disabled: false
};

function JsonSchemaFormFieldGroup(_ref) {
  var fields = _ref.fields,
    disabled = _ref.disabled,
    renderAll = _ref.renderAll;
  var form = useForm();
  if (!form) {
    error('JsonSchemaFormFieldGroup: no form context found!');
    return null;
  }
  if (renderAll) {
    return Object.keys(form.schema.properties).map(function (key) {
      return /*#__PURE__*/React__default.createElement(JsonSchemaFormField, {
        key: key,
        name: key,
        disabled: disabled
      });
    });
  }
  if (fields) {
    return fields.map(function (key) {
      return /*#__PURE__*/React__default.createElement(JsonSchemaFormField, {
        key: key,
        name: key,
        disabled: disabled
      });
    });
  }
  warning('JsonSchemaFormFieldGroup: no fields specified for rendering!');
  return null;
}
JsonSchemaFormFieldGroup.propTypes = {
  fields: PropTypes.array,
  renderAll: PropTypes.bool,
  disabled: PropTypes.bool
};
JsonSchemaFormFieldGroup.defaultProps = {
  renderAll: false,
  disabled: false
};

var useStyles = mui.makeStyles({
  name: 'burgerIcon'
})(function (theme) {
  return {
    root: {
      display: 'inline-block',
      cursor: 'pointer',
      padding: '1em',
      verticalAlign: 'middle',
      '& div': {
        width: '35px',
        height: '5px',
        backgroundColor: theme.palette.primary.contrastText,
        margin: '6px 0',
        transition: '.4s',
        borderRadius: '2px'
      }
    },
    rootActive: {
      '& $bar1': {
        transform: 'rotate(-45deg) translate(-9px, 6px)'
      },
      '& $bar2': {
        opacity: 0
      },
      '& $bar3': {
        transform: 'rotate(45deg) translate(-8px, -8px)'
      }
    },
    bar1: {},
    bar2: {},
    bar3: {}
  };
});
function BurgerIcon(_ref) {
  var active = _ref.active,
    onClick = _ref.onClick,
    className = _ref.className;
  var _useStyles = useStyles(),
    classes = _useStyles.classes;
  return /*#__PURE__*/React__default.createElement("div", {
    className: [classes.root, active && classes.rootActive, className].join(' '),
    onClick: onClick
  }, /*#__PURE__*/React__default.createElement("div", {
    className: classes.bar1
  }), /*#__PURE__*/React__default.createElement("div", {
    className: classes.bar2
  }), /*#__PURE__*/React__default.createElement("div", {
    className: classes.bar3
  }));
}
BurgerIcon.propTypes = {
  active: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string
};
BurgerIcon.defaultProps = {
  active: false,
  onClick: function onClick() {}
};

var _excluded$1 = ["value", "children"];
var useStyles$1 = mui.makeStyles({
  name: 'copyButton'
})({
  textarea: {
    opacity: 0,
    width: 0,
    height: 0,
    padding: 0
  }
});
var copyToClipboard = function copyToClipboard(ref, text) {
  if (text === void 0) {
    text = null;
  }
  try {
    var _temp3 = function _temp3() {
      var range = document.createRange();
      range.selectNode(element);
      window.getSelection().addRange(range);
      var successful = false;
      try {
        document.execCommand('copy');
        successful = document.execCommand('copy');
      } catch (err) {
        error("Unable to copy: " + err);
        successful = false;
      }
      window.getSelection().removeAllRanges();
      return successful;
    };
    var element = ref.current;
    if (!isBrowser || !element) return Promise.resolve(false);
    var _temp4 = function () {
      if (text) {
        element.innerHTML = text;
        return Promise.resolve(sleep(1000)).then(function () {});
      }
    }();
    return Promise.resolve(_temp4 && _temp4.then ? _temp4.then(_temp3) : _temp3(_temp4));
  } catch (e) {
    return Promise.reject(e);
  }
};
function CopyButton(_ref) {
  var value = _ref.value,
    children = _ref.children,
    props = _objectWithoutPropertiesLoose(_ref, _excluded$1);
  var _useStyles = useStyles$1(),
    classes = _useStyles.classes;
  var spanRef = React__default.createRef(null);
  var handleClick = function handleClick() {
    copyToClipboard(spanRef);
  };
  return /*#__PURE__*/React__default.createElement(React__default.Fragment, null, /*#__PURE__*/React__default.createElement("span", {
    ref: spanRef,
    className: classes.textarea
  }, value), /*#__PURE__*/React__default.createElement(material.Button, _extends({
    onClick: handleClick
  }, props), children));
}
CopyButton.propTypes = {
  value: PropTypes.string,
  children: PropTypes.node.isRequired
};

var _excluded$2 = ["noIcon", "children"];
var useStyles$2 = mui.makeStyles({
  name: 'externalLink'
})({
  icon: {
    verticalAlign: 'middle',
    width: 16,
    height: 16,
    marginLeft: 2
  }
});
function ExternalLink(_ref) {
  var noIcon = _ref.noIcon,
    children = _ref.children,
    props = _objectWithoutPropertiesLoose(_ref, _excluded$2);
  var _useStyles = useStyles$2(),
    classes = _useStyles.classes;
  return /*#__PURE__*/React__default.createElement(material.Link, _extends({}, props, {
    target: "_blank",
    rel: "noopener noreferrer"
  }), children, !noIcon && /*#__PURE__*/React__default.createElement(LaunchIcon, {
    className: classes.icon
  }));
}
ExternalLink.propTypes = {
  href: PropTypes.string.isRequired,
  color: PropTypes.string,
  noIcon: PropTypes.bool,
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};
ExternalLink.defaultProps = {
  color: 'secondary',
  noIcon: false
};

var FileIcon = createSvgIcon( /*#__PURE__*/React__default.createElement("path", {
  d: "M14,17H7V15H14M17,13H7V11H17M17,9H7V7H17M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3Z"
}), 'File');

var PdfIcon = createSvgIcon( /*#__PURE__*/React__default.createElement("path", {
  d: "M11.43,10.94C11.2,11.68 10.87,12.47 10.42,13.34C10.22,13.72 10,14.08 9.92,14.38L10.03,14.34V14.34C11.3,13.85 12.5,13.57 13.37,13.41C13.22,13.31 13.08,13.2 12.96,13.09C12.36,12.58 11.84,11.84 11.43,10.94M17.91,14.75C17.74,14.94 17.44,15.05 17,15.05C16.24,15.05 15,14.82 14,14.31C12.28,14.5 11,14.73 9.97,15.06C9.92,15.08 9.86,15.1 9.79,15.13C8.55,17.25 7.63,18.2 6.82,18.2C6.66,18.2 6.5,18.16 6.38,18.09L5.9,17.78L5.87,17.73C5.8,17.55 5.78,17.38 5.82,17.19C5.93,16.66 6.5,15.82 7.7,15.07C7.89,14.93 8.19,14.77 8.59,14.58C8.89,14.06 9.21,13.45 9.55,12.78C10.06,11.75 10.38,10.73 10.63,9.85V9.84C10.26,8.63 10.04,7.9 10.41,6.57C10.5,6.19 10.83,5.8 11.2,5.8H11.44C11.67,5.8 11.89,5.88 12.05,6.04C12.71,6.7 12.4,8.31 12.07,9.64C12.05,9.7 12.04,9.75 12.03,9.78C12.43,10.91 13,11.82 13.63,12.34C13.89,12.54 14.18,12.74 14.5,12.92C14.95,12.87 15.38,12.85 15.79,12.85C17.03,12.85 17.78,13.07 18.07,13.54C18.17,13.7 18.22,13.89 18.19,14.09C18.18,14.34 18.09,14.57 17.91,14.75M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3M17.5,14.04C17.4,13.94 17,13.69 15.6,13.69C15.53,13.69 15.46,13.69 15.37,13.79C16.1,14.11 16.81,14.3 17.27,14.3C17.34,14.3 17.4,14.29 17.46,14.28H17.5C17.55,14.26 17.58,14.25 17.59,14.15C17.57,14.12 17.55,14.08 17.5,14.04M8.33,15.5C8.12,15.62 7.95,15.73 7.85,15.81C7.14,16.46 6.69,17.12 6.64,17.5C7.09,17.35 7.68,16.69 8.33,15.5M11.35,8.59L11.4,8.55C11.47,8.23 11.5,7.95 11.56,7.73L11.59,7.57C11.69,7 11.67,6.71 11.5,6.47L11.35,6.42C11.33,6.45 11.3,6.5 11.28,6.54C11.11,6.96 11.12,7.69 11.35,8.59Z"
}), 'PDF');

var ImageIcon = createSvgIcon( /*#__PURE__*/React__default.createElement("path", {
  d: "M8.5,13.5L11,16.5L14.5,12L19,18H5M21,19V5C21,3.89 20.1,3 19,3H5A2,2 0 0,0 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19Z"
}), 'Image');

var ArchiveIcon = createSvgIcon( /*#__PURE__*/React__default.createElement("path", {
  d: "M5.12,5L5.93,4H17.93L18.87,5M12,17.5L6.5,12H10V10H14V12H17.5L12,17.5M20.54,5.23L19.15,3.55C18.88,3.21 18.47,3 18,3H6C5.53,3 5.12,3.21 4.84,3.55L3.46,5.23C3.17,5.57 3,6 3,6.5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V6.5C21,6 20.83,5.57 20.54,5.23Z"
}), 'Archive');

var WordIcon = createSvgIcon( /*#__PURE__*/React__default.createElement("path", {
  d: "M15.5,17H14L12,9.5L10,17H8.5L6.1,7H7.8L9.34,14.5L11.3,7H12.7L14.67,14.5L16.2,7H17.9M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3Z"
}), 'Word');

var ExcelIcon = createSvgIcon( /*#__PURE__*/React__default.createElement("path", {
  d: "M16.2,17H14.2L12,13.2L9.8,17H7.8L11,12L7.8,7H9.8L12,10.8L14.2,7H16.2L13,12M19,3H5C3.89,3 3,3.89 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19V5C21,3.89 20.1,3 19,3Z"
}), 'Excel');

var PowerPointIcon = createSvgIcon( /*#__PURE__*/React__default.createElement("path", {
  d: "M9.8,13.4H12.3C13.8,13.4 14.46,13.12 15.1,12.58C15.74,12.03 16,11.25 16,10.23C16,9.26 15.75,8.5 15.1,7.88C14.45,7.29 13.83,7 12.3,7H8V17H9.8V13.4M19,3A2,2 0 0,1 21,5V19A2,2 0 0,1 19,21H5A2,2 0 0,1 3,19V5C3,3.89 3.9,3 5,3H19M9.8,12V8.4H12.1C12.76,8.4 13.27,8.65 13.6,9C13.93,9.35 14.1,9.72 14.1,10.24C14.1,10.8 13.92,11.19 13.6,11.5C13.28,11.81 12.9,12 12.22,12H9.8Z"
}), 'PowerPoint');

var _excluded$3 = ["mimeType"];
var getIconComponentByMimeType = function getIconComponentByMimeType(mimeType) {
  switch (mimeType) {
    case 'application/pdf':
      return PdfIcon;
    case 'application/vnd.oasis.opendocument.text':
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    case 'application/msword':
      return WordIcon;
    case 'application/vnd.oasis.opendocument.presentation':
    case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
    case 'application/vnd.ms-powerpoint':
      return PowerPointIcon;
    case 'application/vnd.oasis.opendocument.spreadsheet':
    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
    case 'application/vnd.ms-excel':
      return ExcelIcon;
    case 'application/x-tar':
    case 'application/octet-stream':
    case 'application/x-bzip':
    case 'application/x-bzip2':
    case 'application/x-rar-compressed':
    case 'application/zip':
    case 'application/x-7z-compressed':
      return ArchiveIcon;
    case 'image/jpeg':
    case 'image/gif':
    case 'image/png':
    case 'image/svg+xml':
      return ImageIcon;
    default:
      return FileIcon;
  }
};
function FileTypeIcon(_ref) {
  var mimeType = _ref.mimeType,
    props = _objectWithoutPropertiesLoose(_ref, _excluded$3);
  var IconComponent = getIconComponentByMimeType(mimeType);
  return /*#__PURE__*/React__default.createElement(IconComponent, props);
}
FileTypeIcon.propTypes = {
  mimeType: PropTypes.string
};

var _excluded$4 = ["title"];
var useStyles$3 = mui.makeStyles({
  name: 'helpTooltip'
})({
  icon: {
    verticalAlign: 'middle',
    width: 16,
    height: 16,
    marginLeft: 2,
    position: 'relative',
    top: '-1px'
  },
  tooltip: {
    fontSize: '.9em'
  }
});
function HelpTooltip(_ref) {
  var title = _ref.title,
    props = _objectWithoutPropertiesLoose(_ref, _excluded$4);
  var _useStyles = useStyles$3(),
    classes = _useStyles.classes;
  return /*#__PURE__*/React__default.createElement(material.Tooltip, _extends({
    title: title,
    classes: {
      tooltip: classes.tooltip
    }
  }, props), /*#__PURE__*/React__default.createElement(HelpOutlineIcon, {
    className: classes.icon,
    fontSize: "small"
  }));
}
HelpTooltip.propTypes = {
  title: PropTypes.node
};

function HideOnScroll(_ref) {
  var enabled = _ref.enabled,
    direction = _ref.direction,
    children = _ref.children;
  var trigger = material.useScrollTrigger();
  var triggered = direction === 'down' && trigger || direction === 'up' && !trigger;
  return /*#__PURE__*/React__default.createElement(Slide, {
    appear: false,
    direction: direction,
    "in": !enabled || !triggered
  }, children);
}
HideOnScroll.defaultProps = {
  direction: 'down'
};
HideOnScroll.propTypes = {
  enabled: PropTypes.bool,
  direction: PropTypes.oneOf(['down', 'up']),
  children: PropTypes.node.isRequired
};

var _excluded$5 = ["src", "ratioX", "ratioY", "alt", "className", "loadingPlaceholder", "missingText", "errorText"];
var useStyles$4 = mui.makeStyles({
  name: 'image'
})(function (theme) {
  return {
    root: {
      width: '100%',
      backgroundColor: theme.palette.common.grey,
      position: 'relative',
      '& > *': {
        maxWidth: '100%',
        maxHeight: '100%'
      }
    },
    centered: {
      position: 'absolute !important',
      top: '50%',
      left: '50%',
      transform: 'translateX(-50%) translateY(-50%)'
    },
    "static": {
      width: '100%',
      height: '100%'
    },
    hidden: {
      visibility: 'hidden'
    },
    error: {
      '& > *': {
        display: 'inline-block',
        verticalAlign: 'middle',
        marginLeft: '.5em'
      }
    }
  };
});
function Image(_ref) {
  var src = _ref.src,
    ratioX = _ref.ratioX,
    ratioY = _ref.ratioY,
    alt = _ref.alt,
    className = _ref.className,
    loadingPlaceholder = _ref.loadingPlaceholder,
    missingText = _ref.missingText,
    errorText = _ref.errorText,
    props = _objectWithoutPropertiesLoose(_ref, _excluded$5);
  var _useState = React.useState(false),
    loaded = _useState[0],
    setLoaded = _useState[1];
  var _useState2 = React.useState(false),
    error = _useState2[0],
    setError = _useState2[1];
  var _useStyles = useStyles$4(),
    classes = _useStyles.classes;
  var ratio = ratioY / ratioX * 100;
  var handleImageLoaded = function handleImageLoaded() {
    setLoaded(true);
    setError(false);
  };
  var handleImageError = function handleImageError() {
    setLoaded(true);
    setError(true);
  };
  if (!src) {
    return /*#__PURE__*/React__default.createElement("div", _extends({
      className: [classes.root, className].join(' '),
      style: {
        paddingBottom: ratio + "%"
      }
    }, props), /*#__PURE__*/React__default.createElement("div", {
      className: [classes.centered, classes.error].join(' ')
    }, /*#__PURE__*/React__default.createElement(ErrorOutlineIcon, null), /*#__PURE__*/React__default.createElement(material.Typography, null, missingText)));
  }
  return /*#__PURE__*/React__default.createElement("div", _extends({
    className: [classes.root, className].join(' '),
    style: {
      paddingBottom: ratio + "%"
    }
  }, props), /*#__PURE__*/React__default.createElement("img", {
    src: src,
    alt: alt,
    className: [classes.centered, !loaded || error ? classes.hidden : undefined].join(' '),
    onLoad: handleImageLoaded,
    onError: handleImageError
  }), !loaded && (loadingPlaceholder || /*#__PURE__*/React__default.createElement(material.Skeleton, {
    className: classes.centered,
    width: "100%",
    height: "100%",
    variant: "rect",
    animation: "wave"
  })), loaded && error && /*#__PURE__*/React__default.createElement("div", {
    className: [classes.centered, classes.error].join(' ')
  }, /*#__PURE__*/React__default.createElement(ErrorOutlineIcon, null), /*#__PURE__*/React__default.createElement(material.Typography, null, errorText)));
}
Image.defaultProps = {
  missingText: 'missing.image',
  errorText: 'error.image'
};
Image.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  ratioX: PropTypes.number.isRequired,
  ratioY: PropTypes.number.isRequired,
  loadingPlaceholder: PropTypes.node,
  missingText: PropTypes.string,
  errorText: PropTypes.string,
  className: PropTypes.string
};

var _excluded$6 = ["children", "component"];
function Link(_ref) {
  var children = _ref.children,
    LinkComponent = _ref.component,
    props = _objectWithoutPropertiesLoose(_ref, _excluded$6);
  var MyLinkComponent = React__default.forwardRef(function (nestedProps, ref) {
    return /*#__PURE__*/React__default.createElement(LinkComponent, nestedProps);
  });
  MyLinkComponent.displayName = 'MyLinkComponent';
  return /*#__PURE__*/React__default.createElement(material.Link, _extends({}, props, {
    component: MyLinkComponent
  }), children);
}
Link.defaultProps = {
  color: 'secondary'
};
Link.propTypes = {
  href: PropTypes.string.isRequired,
  language: PropTypes.string,
  color: PropTypes.string,
  children: PropTypes.node.isRequired,
  component: PropTypes.elementType.isRequired,
  className: PropTypes.string
};

var _excluded$7 = ["value", "onChange", "onSubmit", "placeholder", "aria-label", "className"];
var useStyles$5 = mui.makeStyles({
  name: 'searchField'
})(function (theme) {
  return {
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      background: theme.palette.common.grey
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1
    },
    iconButton: {
      padding: 10
    },
    iconButtonInvisible: {
      visibility: 'hidden'
    }
  };
});
function SearchField(_ref) {
  var value = _ref.value,
    onChange = _ref.onChange,
    onSubmit = _ref.onSubmit,
    placeholder = _ref.placeholder,
    ariaLabel = _ref['aria-label'],
    props = _objectWithoutPropertiesLoose(_ref, _excluded$7);
  var _useStyles = useStyles$5(),
    classes = _useStyles.classes;
  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    onSubmit(e);
  };
  var handleChange = function handleChange(e) {
    onChange(e);
  };
  var handleClear = function handleClear() {
    onChange({
      target: {
        value: ''
      }
    });
  };
  return /*#__PURE__*/React__default.createElement(Paper, _extends({
    className: classes.root
  }, props), /*#__PURE__*/React__default.createElement(InputBase, {
    className: classes.input,
    placeholder: placeholder,
    value: value,
    onChange: handleChange,
    inputProps: {
      'aria-label': ariaLabel
    }
  }), /*#__PURE__*/React__default.createElement(IconButton, {
    type: "button",
    className: [classes.iconButton, !value && classes.iconButtonInvisible].join(' '),
    onClick: handleClear,
    "aria-label": "clear"
  }, /*#__PURE__*/React__default.createElement(ClearIcon, null)), onSubmit && /*#__PURE__*/React__default.createElement(IconButton, {
    type: "submit",
    className: classes.iconButton,
    onClick: handleSubmit,
    "aria-label": "search"
  }, /*#__PURE__*/React__default.createElement(SearchIcon, null)));
}
SearchField.defaultProps = {
  onChange: function onChange() {}
};
SearchField.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  value: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  'aria-label': PropTypes.string,
  className: PropTypes.string
};

var _excluded$8 = ["id", "label", "value", "getOptionLabel", "createEntryText", "createEntryTextShort", "disabled", "options", "onChange", "onOpen", "onClose", "clearText", "aria-label", "className"];
var useStyles$6 = mui.makeStyles({
  name: 'selectTextField'
})(function (theme) {
  var _option;
  return {
    root: {
      display: 'flex'
    },
    clearIndicator: {
      marginRight: -2,
      padding: 4,
      color: theme.palette.action.active,
      visibility: 'hidden'
    },
    clearIndicatorDirty: {
      visibility: 'visible'
    },
    option: (_option = {
      minHeight: 48,
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      cursor: 'pointer',
      paddingTop: 6,
      boxSizing: 'border-box',
      outline: '0',
      WebkitTapHighlightColor: 'transparent',
      paddingBottom: 6,
      paddingLeft: 16,
      paddingRight: 16
    }, _option[theme.breakpoints.up('sm')] = {
      minHeight: 'auto'
    }, _option['&[aria-selected="true"]'] = {
      backgroundColor: theme.palette.action.selected
    }, _option['&[data-focus="true"]'] = {
      backgroundColor: theme.palette.action.hover
    }, _option['&[aria-disabled="true"]'] = {
      opacity: 0.5,
      pointerEvents: 'none'
    }, _option['&:active'] = {
      backgroundColor: theme.palette.action.selected
    }, _option),
    textfield: {
      width: 'inherit'
    },
    input: {
      flex: 1
    },
    button: {
      marginLeft: theme.spacing(1)
    },
    popper: {
      zIndex: theme.zIndex.modal
    },
    menuList: {
      maxHeight: '50vh',
      overflowY: 'auto'
    }
  };
});
function SelectTextField(_ref) {
  var idProp = _ref.id,
    label = _ref.label,
    value = _ref.value,
    getOptionLabel = _ref.getOptionLabel,
    createEntryText = _ref.createEntryText,
    createEntryTextShort = _ref.createEntryTextShort,
    disabledProp = _ref.disabled,
    options = _ref.options,
    onChange = _ref.onChange,
    onOpen = _ref.onOpen,
    onClose = _ref.onClose,
    clearText = _ref.clearText,
    ariaLabel = _ref['aria-label'],
    props = _objectWithoutPropertiesLoose(_ref, _excluded$8);
  var _useState = React.useState(false),
    createNewEntry = _useState[0],
    setCreateNewEntry = _useState[1];
  var _useState2 = React.useState(value),
    inputValue = _useState2[0],
    setInputValue = _useState2[1];
  var _useState3 = React.useState(false),
    open = _useState3[0],
    setOpen = _useState3[1];
  var _useState4 = React.useState(null),
    anchorElement = _useState4[0],
    setAnchorElement = _useState4[1];
  var firstFocus = React.useRef(true);
  var inputRef = React.useRef(null);
  var _useStyles = useStyles$6(),
    classes = _useStyles.classes;
  var _React$useState = React__default.useState(),
    defaultId = _React$useState[0],
    setDefaultId = _React$useState[1];
  var id = idProp || defaultId;
  React.useEffect(function () {
    setDefaultId("select-textfield-" + Math.round(Math.random() * 1e5));
  }, []);
  React.useEffect(function () {
    setInputValue(value);
  }, [value]);
  var filteredOptions = React.useCallback(function () {
    if (!inputValue) {
      return options;
    }
    var regex = RegExp(".*(" + inputValue + ").*", 'gi');
    return options.filter(function (item) {
      return regex.test(item);
    });
  }, [inputValue]);
  var handleOpen = function handleOpen() {
    if (open) {
      return;
    }
    if (onOpen) {
      onOpen();
    }
    setOpen(true);
  };
  var handleClose = function handleClose() {
    if (!open) {
      return;
    }
    if (onClose) {
      onClose();
    }
    setOpen(false);
    setInputValue(value);
  };
  var handleRootClick = function handleRootClick() {
    if (firstFocus.current && inputRef.current.selectionEnd - inputRef.current.selectionStart === 0) {
      inputRef.current.focus();
    }
    firstFocus.current = false;
  };
  var handleMouseDown = function handleMouseDown(event) {
    if (event.target.getAttribute('id') !== id) {
      event.preventDefault();
    }
  };
  var handleItemSelected = function handleItemSelected(option) {
    onChange({
      target: {
        value: option
      }
    });
    setCreateNewEntry(false);
    handleClose();
  };
  var handleInputChange = function handleInputChange(event) {
    var newValue = event.target.value;
    setInputValue(newValue);
    if (createNewEntry) {
      onChange({
        target: {
          value: newValue || null
        }
      });
    }
    handleOpen();
  };
  var handleClear = function handleClear() {
    setInputValue(null);
    onChange({
      target: {
        value: null
      }
    });
  };
  var handleCreateNewEntryClicked = function handleCreateNewEntryClicked() {
    setCreateNewEntry(true);
    onChange({
      target: {
        value: inputValue || null
      }
    });
  };
  var disabled = disabledProp || !!(value && inputValue && !createNewEntry);
  var dirty = inputValue && inputValue.length > 0;
  var renderListOption = function renderListOption(option, index) {
    return /*#__PURE__*/React__default.createElement(material.MenuItem, {
      key: index,
      onClick: function onClick() {
        handleItemSelected(option);
      }
    }, /*#__PURE__*/React__default.createElement(material.Typography, {
      variant: "inherit"
    }, getOptionLabel(option)));
  };
  return /*#__PURE__*/React__default.createElement("div", _extends({
    className: classes.root,
    onMouseDown: handleMouseDown,
    onClick: handleRootClick
  }, props), /*#__PURE__*/React__default.createElement(material.TextField, {
    id: id,
    label: label,
    "aria-label": ariaLabel,
    value: inputValue || '',
    disabled: disabled,
    onFocus: handleOpen,
    onBlur: handleClose,
    onChange: handleInputChange,
    className: classes.textfield,
    InputProps: {
      ref: setAnchorElement,
      className: classes.inputRoot,
      autoComplete: 'nope',
      autoCapitalize: 'none',
      spellCheck: 'false',
      endAdornment: /*#__PURE__*/React__default.createElement("div", {
        className: classes.endAdornment
      }, createNewEntry && /*#__PURE__*/React__default.createElement(material.Button, {
        disabled: true
      }, createEntryTextShort), /*#__PURE__*/React__default.createElement(material.IconButton, {
        "aria-label": clearText,
        title: clearText,
        className: [classes.clearIndicator, dirty ? classes.clearIndicatorDirty : null].join(' '),
        onClick: handleClear
      }, /*#__PURE__*/React__default.createElement(CloseIcon, null)))
    },
    inputProps: {
      ref: inputRef,
      className: classes.input,
      disabled: disabled
    }
  }), open && anchorElement && (!createNewEntry || filteredOptions().length > 0) ? /*#__PURE__*/React__default.createElement(material.Popper, {
    className: classes.popper,
    style: {
      width: anchorElement ? anchorElement.clientWidth : null
    },
    role: "presentation",
    anchorEl: anchorElement,
    open: true
  }, /*#__PURE__*/React__default.createElement(material.Paper, {
    className: classes.paper
  }, /*#__PURE__*/React__default.createElement(material.MenuList, {
    className: classes.menuList
  }, filteredOptions().map(function (option, index) {
    return renderListOption(option, index);
  }), !createNewEntry && /*#__PURE__*/React__default.createElement(material.MenuItem, {
    onClick: handleCreateNewEntryClicked
  }, /*#__PURE__*/React__default.createElement(material.Typography, {
    variant: "inherit"
  }, createEntryText))))) : null);
}
SelectTextField.defaultProps = {
  onChange: function onChange() {},
  getOptionLabel: function getOptionLabel(x) {
    return x;
  },
  clearText: 'clear',
  createEntryText: 'Create a new entry',
  createEntryTextShort: 'New entry'
};
SelectTextField.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  value: PropTypes.string,
  'aria-label': PropTypes.string,
  getOptionLabel: PropTypes.func,
  clearText: PropTypes.string,
  createEntryText: PropTypes.string,
  createEntryTextShort: PropTypes.string
};

var useStyles$7 = mui.makeStyles({
  name: 'amivSpinner'
})(function (theme, _ref) {
  var size = _ref.size,
    background = _ref.background;
  return {
    root: {
      width: size * 1.2,
      height: size * 1.2,
      borderRadius: '50%',
      background: background,
      padding: size * 0.1,
      opacity: '0'
    },
    rootCentered: {
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translateX(-50%) translateY(-50%)'
    },
    rootVisible: {
      opacity: '1'
    },
    container: {
      width: size,
      height: size,
      position: 'relative'
    },
    icons: {
      position: 'absolute',
      top: '0',
      left: '0',
      right: '0',
      bottom: '0',
      color: '#e8462b',
      width: '100%',
      height: '100%'
    },
    gear: {
      animationName: '$spin',
      animationDuration: '2500ms',
      animationIterationCount: 'infinite',
      animationTimingFunction: 'ease'
    },
    '@keyframes spin': {
      from: {
        transform: 'rotate(0deg)'
      },
      to: {
        transform: 'rotate(360deg)'
      }
    },
    '@keyframes popup': {
      from: {
        opacity: 0
      },
      '90%': {
        opacity: 0
      },
      to: {
        opacity: '100%'
      }
    }
  };
});
function Spinner(_ref2) {
  var size = _ref2.size,
    centered = _ref2.centered,
    show = _ref2.show,
    background = _ref2.background,
    elevation = _ref2.elevation,
    className = _ref2.className;
  var _useStyles = useStyles$7({
      size: size,
      background: background
    }),
    classes = _useStyles.classes;
  return /*#__PURE__*/React__default.createElement(material.Box, {
    boxShadow: elevation,
    className: [classes.root, centered && classes.rootCentered, show && classes.rootVisible, className].join(' ')
  }, /*#__PURE__*/React__default.createElement("div", {
    className: classes.container
  }, /*#__PURE__*/React__default.createElement("svg", {
    width: size,
    height: size,
    viewBox: "0 0 48 48"
  }, /*#__PURE__*/React__default.createElement("style", {
    type: "text/css"
  }, ".st0", '{fill:#E8462B;}'), /*#__PURE__*/React__default.createElement("path", {
    className: "st0",
    d: "M24.6,20.2l-12.8,1.3l3.8,5.3l-6,4.4c0,0.9,1.9,3.7,3.2,4.5l6.1-4.4l4.1,5.6l5.1-11.8l3.8,5.2l4.5-3.2l-4-5.6l5.2-3.8c-0.8-1.7-1.9-3.2-3.2-4.5L29.2,17l-4-5.6l-4.5,3.2l4.1,5.6l0,0"
  })), /*#__PURE__*/React__default.createElement("svg", {
    width: size,
    height: size,
    viewBox: "0 0 48 48",
    className: [classes.icons, classes.gear].join(' ')
  }, /*#__PURE__*/React__default.createElement("style", {
    type: "text/css"
  }, ".st0", '{fill:#E8462B;}'), /*#__PURE__*/React__default.createElement("path", {
    className: "st0",
    d: "M48.6,27.3c0.3-2.2,0.3-4.3,0-6.5l-5.7-0.9c-0.3-1.2-0.6-2.3-1.1-3.4l4.1-4.1c-0.5-1-1.1-1.9-1.7-2.8c-0.6-0.9-1.3-1.7-2.1-2.5l-5.2,2.7c-0.4-0.4-0.9-0.8-1.4-1.1c-0.5-0.4-1-0.7-1.5-1L35,2c-1-0.5-2-0.9-3-1.2c-1-0.3-2.1-0.6-3.2-0.8l-2.6,5.2c-1.2-0.1-2.3-0.1-3.5,0L20.1,0c-1.1,0.2-2.1,0.5-3.2,0.8c-1,0.3-2,0.8-3,1.2l0.9,5.7c-1,0.6-2,1.3-2.9,2.1L6.7,7.2C6,8,5.3,8.8,4.7,9.7c-0.6,0.9-1.2,1.8-1.7,2.8l4.1,4.1c-0.5,1.1-0.8,2.2-1.1,3.3l-5.8,0.9c-0.3,2.2-0.3,4.3,0,6.5L6,28.2c0.3,1.1,0.6,2.3,1.1,3.3l-4.1,4.1c0.5,1,1.1,1.9,1.7,2.8C5.3,39.3,6,40.2,6.8,41l5.2-2.7c0.4,0.4,0.9,0.8,1.4,1.1c0.5,0.4,1,0.7,1.5,1l-0.9,5.7c1,0.5,2,0.9,3,1.2c1,0.3,2.1,0.6,3.2,0.8l2.6-5.2c1.2,0.1,2.3,0.1,3.5,0l2.6,5.2c1.1-0.2,2.1-0.5,3.2-0.8c1-0.3,2-0.7,3-1.2L34,40.4c1-0.6,2-1.3,2.9-2.1l5.2,2.7c0.8-0.8,1.5-1.6,2.1-2.5c0.6-0.9,1.2-1.8,1.7-2.8l-4.1-4.1c0.5-1.1,0.8-2.2,1.1-3.4L48.6,27.3z M35.5,32.1c-0.1,0.2-0.3,0.4-0.4,0.5s-0.1,0.1-0.1,0.2c-0.2,0.3-0.5,0.6-0.8,0.8l-0.1,0.1c-0.3,0.3-0.5,0.5-0.8,0.8l-0.2,0.1c-0.3,0.2-0.6,0.4-0.9,0.6L32,35.5L31.1,36l-0.4,0.2c-0.2,0.1-0.5,0.2-0.8,0.3l-0.5,0.2L28.8,37l-0.7,0.2l-0.6,0.2c-0.2,0.1-0.5,0.1-0.7,0.2l-0.6,0.1l-0.7,0.1h-0.7h-0.4h-0.2h-0.8l-0.6,0l-0.7-0.1c-0.2,0-0.4-0.1-0.6-0.1s-0.4-0.1-0.6-0.2l-0.7-0.2c-0.2-0.1-0.4-0.1-0.6-0.2s-0.5-0.2-0.7-0.3l-0.5-0.2L17.7,36c-0.2-0.1-0.3-0.2-0.5-0.3s-0.5-0.3-0.7-0.5l-0.2-0.1c-0.1-0.1-0.2-0.1-0.2-0.2c-0.2-0.2-0.5-0.4-0.7-0.6l-0.2-0.1l0,0c-2.7-2.5-4.4-5.9-4.5-9.6v-0.1c0-0.2,0-0.3,0-0.5c0-0.6,0.1-1.3,0.1-1.9l0.1-0.4c0-0.2,0.1-0.5,0.1-0.7s0.1-0.4,0.1-0.6s0.2-0.6,0.3-0.9s0.1-0.3,0.2-0.5s0.1-0.4,0.2-0.5s0.2-0.5,0.4-0.8s0.2-0.4,0.3-0.6s0.2-0.4,0.3-0.6l0.3-0.4l0.1-0.2l0.2-0.2c0.2-0.2,0.3-0.5,0.5-0.7l0.4-0.4c0.2-0.2,0.3-0.4,0.5-0.5s0.3-0.3,0.5-0.4s0.4-0.3,0.6-0.5l0.5-0.4l0.6-0.4l0.5-0.3l0.7-0.4l0.4-0.2l0.8-0.3l0.4-0.1c0.3-0.1,0.6-0.2,0.9-0.3l0.4-0.1c0.3-0.1,0.6-0.1,0.8-0.2c0.2,0,0.3-0.1,0.5-0.1l0.8-0.1H24h0.4h0.2c0.3,0,0.5,0,0.8,0H26l0.9,0.1l0.5,0.1c0.3,0.1,0.6,0.1,0.9,0.2l0.4,0.1c0.3,0.1,0.6,0.2,0.9,0.3l0.3,0.1c0.3,0.1,0.6,0.3,0.9,0.4l0.3,0.1c0.3,0.2,0.6,0.3,0.8,0.5c0.1,0.1,0.2,0.1,0.3,0.2l0.3,0.2l0.4,0.3c0.2,0.1,0.3,0.3,0.5,0.4c0.1,0.1,0.2,0.2,0.3,0.3l0,0c1.4,1.3,2.4,2.8,3.2,4.5l0,0c0,0,0,0.1,0,0.1c0.1,0.2,0.1,0.3,0.2,0.5s0.2,0.4,0.2,0.6s0.1,0.3,0.2,0.5s0.2,0.6,0.2,0.9s0.1,0.3,0.1,0.5s0.1,0.6,0.1,0.8c0,0.1,0,0.2,0,0.3c0,0.4,0.1,0.8,0.1,1.2c0,0,0,0.1,0,0.1s0,0.1,0,0.1C38.1,27,37.2,29.8,35.5,32.1z"
  }))));
}
Spinner.defaultProps = {
  elevation: 0,
  background: null,
  centered: false,
  size: 48,
  show: true
};
Spinner.propTypes = {
  size: PropTypes.number,
  centered: PropTypes.bool,
  show: PropTypes.bool,
  background: PropTypes.string,
  elevation: PropTypes.number,
  className: PropTypes.string
};

exports.BurgerIcon = BurgerIcon;
exports.CopyButton = CopyButton;
exports.ExternalLink = ExternalLink;
exports.FileTypeIcon = FileTypeIcon;
exports.Form = JsonSchemaForm;
exports.HelpTooltip = HelpTooltip;
exports.HideOnScroll = HideOnScroll;
exports.Image = Image;
exports.JsonSchemaForm = JsonSchemaForm;
exports.JsonSchemaFormField = JsonSchemaFormField;
exports.JsonSchemaFormFieldGroup = JsonSchemaFormFieldGroup;
exports.Link = Link;
exports.Query = Query;
exports.SearchField = SearchField;
exports.SelectTextField = SelectTextField;
exports.Spinner = Spinner;
exports.copyToClipboard = copyToClipboard;
exports.error = error;
exports.isBrowser = isBrowser;
exports.isDevEnvironment = isDevEnvironment;
exports.isObject = isObject;
exports.log = log;
exports.sleep = sleep;
exports.warning = warning;
//# sourceMappingURL=index.js.map
