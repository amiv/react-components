# amiv React Components

[![pipeline status](https://gitlab.ethz.ch/amiv/react-components/badges/master/pipeline.svg)](https://gitlab.ethz.ch/amiv/react-components/commits/master)

This is a collection of reusable React components for use within amiv React applications.

## 🚀 Quick start

1. **Getting Started / Usage**

   Install the component library with

   ```shell
   yarn add git+https://gitlab.ethz.ch/amiv/react-components.git#<commit-hash>
   ```

   *Please note that the commit hash is optional*

   Import the components from `amiv-react-components`.

2. **Initialize Git LFS**

   When you clone this repository, you have to initialize git LFS (Large-File-System) for handling binary files.

   1. Make sure that git-lfs is installed on your system. On Ubuntu, you can install it with `apt install git-lfs`.
   2. Execute `git lfs install` to install the git hooks in the local repository.

3. **Add a new component**

   Create the component file at `src/components` and add an export to the file `./index.js`. Make sure to execute `npm run build` before committing. Otherwise the CI Pipeline tests will fail and the Merge Request will be blocked.

4. **Add a new tutorial**

   Create a new markdown file at `docs/`. Make sure to name the file the same as the title of the tutorial should be!

5. **Linting & Formatting**

   We use `eslint` and `prettier` for linting and formatting of the javascript code. You can use the following commands:

   ```shell
   yarn run eslint
   yarn run format
   ```

## 🧐 What's inside?

A quick look at the most important files and directories of the project.

```ascii
.
├── jsdoc.json
├── docs/
└── src/
    ├── components/
    ├── utils/
    └── index.js
```

1. **`/jsdoc.js`**: [JSDoc](https://jsdoc.app/) configuration file

2. **`/docs`**: Contains tutorial files (`.md`) included in the JSDoc documentation website.

3. **`/src`**: This directory contains all source code for the components

   1. **`/src/components`**: Contains all react components.

   2. **`/src/utils`**: Contains all utility functions and classes.

   3. **`/src/index.js`**: Entry point for the package. Exports all components.

## 💫 Documentation (Deployment)

The documentation of this package is built as a docker image. See CI Pipeline for details.

## ⚙ Technologies

### Frontend Frameworks & Libraries

* [React](https://reactjs.org/)
* [Material-UI](https://material-ui.com/)

### Build Tools

* [Yarn](https://yarnpkg.com/)
* [JSDoc](https://jsdoc.app/)

### Development Tools

* [ESlint](https://github.com/eslint/eslint)
* [Prettier](https://github.com/prettier/prettier)

  Most IDEs have plugins for those tools. VS Code is the recommended IDE.
