# This Dockerfile uses a multi-stage build. Only the first stage requires
# all dependencies, the final image will contain only the output files

# First stage: Build project
FROM node:16 as build

ARG COMMIT_SHA="<commit-hash>"

# Copy files and install dependencies
COPY ./ /react-components/

WORKDIR /react-components

# Install dependencies
RUN yarn install

# Prepare README.md & build project
RUN sed -i s/\<commit\-hash\>/$COMMIT_SHA/ README.md
RUN yarn run build-docs

# Second stage: Server to deliver files
FROM nginx:1.15-alpine

# Copy files from first stage
COPY --from=build /react-components/build /var/www

# Copy nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf
