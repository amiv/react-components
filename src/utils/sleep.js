/**
 * Pause execution for some amount of time.
 *
 * Returns a Promise which will be resolved after the given milliseconds have passed.
 *
 * @param {number} milliseconds time to sleep
 */
const sleep = milliseconds => {
  return new Promise(resolve => {
    setTimeout(resolve, milliseconds)
  })
}

export default sleep
