/**
 * Check if execution is running in a browser.
 */
const isBrowser = () => typeof window !== `undefined`

/**
 * Check whether a given variable is an object.
 *
 * @param {any} value variable to be checked
 */
const isObject = value => typeof value === 'object' && value !== null

/**
 * Check if the application is currently running in development mode.
 */
const isDevEnvironment = () =>
  process.env.NODE_ENV !== 'production' &&
  !process.env.REACT_PRODUCTION_DEBUGGING

export { isBrowser, isObject, isDevEnvironment }
