import React from 'react'
import PropTypes from 'prop-types'

import FileIcon from './icons/file'
import PdfIcon from './icons/pdf'
import ImageIcon from './icons/image'
import ArchiveIcon from './icons/archive'
import WordIcon from './icons/word'
import ExcelIcon from './icons/excel'
import PowerPointIcon from './icons/powerpoint'

const getIconComponentByMimeType = mimeType => {
  switch (mimeType) {
    case 'application/pdf':
      return PdfIcon
    // Word Documents
    case 'application/vnd.oasis.opendocument.text':
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
    case 'application/msword':
      return WordIcon
    // PowerPoint Documents
    case 'application/vnd.oasis.opendocument.presentation':
    case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
    case 'application/vnd.ms-powerpoint':
      return PowerPointIcon
    // Excel Document
    case 'application/vnd.oasis.opendocument.spreadsheet':
    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
    case 'application/vnd.ms-excel':
      return ExcelIcon
    // Archives
    case 'application/x-tar':
    case 'application/octet-stream':
    case 'application/x-bzip':
    case 'application/x-bzip2':
    case 'application/x-rar-compressed':
    case 'application/zip':
    case 'application/x-7z-compressed':
      return ArchiveIcon
    // Images
    case 'image/jpeg':
    case 'image/gif':
    case 'image/png':
    case 'image/svg+xml':
      return ImageIcon
    default:
      return FileIcon
  }
}

/**
 * Icon reflecting a given MIME type.
 *
 * Will use default file icon if MIME type is not known.
 *
 * @component
 */
function FileTypeIcon({ mimeType, ...props }) {
  const IconComponent = getIconComponentByMimeType(mimeType)
  return <IconComponent {...props} />
}

FileTypeIcon.propTypes = {
  /** MimeType to select corresponding icon */
  mimeType: PropTypes.string,
}

export default FileTypeIcon
