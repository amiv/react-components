import React from 'react'
import PropTypes from 'prop-types'

import { useForm } from './form'
import JsonSchemaFormField from './field'
import { warning, error } from '../../utils/log'

/**
 * JSONSchema form field group
 *
 * Renders some specified fields of a JSONSchema form.
 *
 * @component
 */
function JsonSchemaFormFieldGroup({ fields, disabled, renderAll }) {
  const form = useForm()

  if (!form) {
    error('JsonSchemaFormFieldGroup: no form context found!')
    return null
  }

  if (renderAll) {
    return Object.keys(form.schema.properties).map(key => (
      <JsonSchemaFormField key={key} name={key} disabled={disabled} />
    ))
  }

  if (fields) {
    return fields.map(key => (
      <JsonSchemaFormField key={key} name={key} disabled={disabled} />
    ))
  }

  warning('JsonSchemaFormFieldGroup: no fields specified for rendering!')
  return null
}

JsonSchemaFormFieldGroup.propTypes = {
  /**
   * Fields to be rendered.
   * Ignored when `renderAll` is set to `true`.
   */
  fields: PropTypes.array,
  /** If `true`, renders all fields of the form. */
  renderAll: PropTypes.bool,
  /** Disables all rendered fields. */
  disabled: PropTypes.bool,
}

JsonSchemaFormFieldGroup.defaultProps = {
  renderAll: false,
  disabled: false,
}

export default JsonSchemaFormFieldGroup
