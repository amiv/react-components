import React from 'react'
import PropTypes from 'prop-types'
import {
  FormControlLabel,
  FormControl,
  InputLabel,
  Select,
  TextField,
  RadioGroup,
  Radio,
  Checkbox,
} from '@mui/material'

import { useForm } from './form'
import { warning } from '../../utils/log'

/**
 * JSONSchema form field
 *
 * @component
 */
function JsonSchemaFormField({
  name,
  label: labelProp,
  description: descriptionProp,
  disabled,
  ...props
}) {
  const {
    schema,
    enumSelectThreshold,
    values,
    onChange,
    renderLabel,
    getErrorsForField,
  } = useForm()

  if (!schema || !schema.properties || !schema.properties[name]) {
    warning(`FormField: property "${name}" not found in FormContext!`)
    return null
  }

  const field = schema.properties[name]
  const errors = getErrorsForField(name)
  const hasErrors = errors.length > 0
  const value = values[name]

  const handleChange = e => {
    onChange({ name, value: e.target.value })
  }

  const label = renderLabel(labelProp || field.label || field.title || name)
  const description = descriptionProp || field.description
  const help = hasErrors ? errors.join(', ') : description

  // Nullable fields are explicitly set to type: ['null', actual type]
  let { type } = field
  if (Array.isArray(field.type)) {
    // takes second element of array
    if (field.type[0] === 'null') [, type] = field.type
    else [type] = field.type // takes first element of array
  }

  if (field.enum) {
    if (field.enum.length < enumSelectThreshold) {
      return (
        <RadioGroup
          name={name}
          value={value}
          onChange={handleChange}
          {...props}
        >
          {field.enum.map(item => (
            <FormControlLabel
              key={item}
              value={item}
              disabled={disabled}
              control={<Radio />}
              label={label}
            />
          ))}
        </RadioGroup>
      )
    }
    // above threshold -> render as Select field
    return (
      <FormControl variant="outlined" {...props}>
        <InputLabel>{label}</InputLabel>
        <Select
          variant="outlined"
          native
          name={name}
          value={value || ''}
          label={label}
          disabled={disabled}
          onChange={handleChange}
        >
          <option key='null' value={null} disabled hidden />
          {field.enum.map(item => (
            <option key={item} value={item}>
              {renderLabel(item)}
            </option>
          ))}
        </Select>
      </FormControl>
    )
  }
  if (type === 'string') {
    if (field.format === 'date-time') {
      return (
        <TextField
          name={name}
          label={label}
          helperText={help}
          error={hasErrors}
          disabled={disabled}
          type='datetime-local'
          value={value || ''}
          onChange={handleChange}
          {...props}
        />
      )
    }
    return (
      <TextField
        name={name}
        label={label}
        helperText={help}
        error={hasErrors}
        disabled={disabled}
        value={value || ''}
        type='text'
        onChange={handleChange}
        {...props}
      />
    )
  }
  if (type === 'number' || type === 'integer') {
    return (
      <TextField
        name={name}
        label={label}
        helperText={help}
        error={hasErrors}
        disabled={disabled}
        value={value || 0}
        type='number'
        onChange={handleChange}
        {...props}
      />
    )
  }
  if (type === 'boolean') {
    return (
      <Checkbox
        name={name}
        checked={value}
        disabled={disabled}
        onChange={e =>
          handleChange({
            ...e,
            target: { ...e.target, value: e.target.checked },
          })
        }
      />
    )
  }
  warning(
    `JsonSchemaForm: field type '${type}' with name '${name}' not supported!`
  )
  return null
}

JsonSchemaFormField.propTypes = {
  /** Identifier for the field to be rendered. */
  name: PropTypes.string.isRequired,
  /** Override the label from the JSON schema. */
  label: PropTypes.string,
  /** Override the description from the JSON schema. */
  description: PropTypes.string,
  /** Disable the form field */
  disabled: PropTypes.bool,
}

JsonSchemaFormField.defaultProps = {
  disabled: false,
}

export default JsonSchemaFormField
