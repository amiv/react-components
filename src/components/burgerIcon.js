import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from 'tss-react/mui'

const useStyles = makeStyles({ name: 'burgerIcon' })(theme => ({
  root: {
    display: 'inline-block',
    cursor: 'pointer',
    padding: '1em',
    verticalAlign: 'middle',

    '& div': {
      width: '35px',
      height: '5px',
      backgroundColor: theme.palette.primary.contrastText,
      margin: '6px 0',
      transition: '.4s',
      borderRadius: '2px',
    },
  },
  rootActive: {
    '& $bar1': {
      transform: 'rotate(-45deg) translate(-9px, 6px)',
    },
    '& $bar2': {
      opacity: 0,
    },
    '& $bar3': {
      transform: 'rotate(45deg) translate(-8px, -8px)',
    },
  },
  bar1: {},
  bar2: {},
  bar3: {},
}))

/**
 * Burger Icon for use as a button for a mobile menu.
 *
 * **Example**
 * ```
 * const [active, setActive] = React.useState(false)
 * return (
 *     <BurgerIcon active={active} onClick={() => setActive(!active))} />
 * )
 *
 * @component
 */
function BurgerIcon({ active, onClick, className }) {
  const { classes } = useStyles()

  return (
    <div
      className={[classes.root, active && classes.rootActive, className].join(
        ' '
      )}
      onClick={onClick}
    >
      <div className={classes.bar1} />
      <div className={classes.bar2} />
      <div className={classes.bar3} />
    </div>
  )
}

BurgerIcon.propTypes = {
  /** Show burger when false, show cross when true */
  active: PropTypes.bool,
  /** Callback when the user clicked on the icon */
  onClick: PropTypes.func,
  /** Additional class applied to the outermost box. */
  className: PropTypes.string,
}

BurgerIcon.defaultProps = {
  active: false,
  onClick: () => {},
}

export default BurgerIcon
