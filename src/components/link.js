import React from 'react'
import PropTypes from 'prop-types'
import { Link as MaterialLink } from '@mui/material'

/**
 * Link component coupled with the styles for `@mui/material`.
 *
 * @component
 */
function Link({ children, component: LinkComponent, ...props }) {
  // eslint-disable-next-line no-unused-vars, react/no-unstable-nested-components
  const MyLinkComponent = React.forwardRef((nestedProps, ref) => {
    return <LinkComponent {...nestedProps} />
  })
  MyLinkComponent.displayName = 'MyLinkComponent'

  return (
    <MaterialLink {...props} component={MyLinkComponent}>
      {children}
    </MaterialLink>
  )
}

Link.defaultProps = {
  color: 'secondary',
}

Link.propTypes = {
  /** Target URL */
  href: PropTypes.string.isRequired,
  /** Target language */
  language: PropTypes.string,
  /** Text color */
  color: PropTypes.string,
  /** Content of the link (text) */
  children: PropTypes.node.isRequired,
  /** Base component for internal links */
  component: PropTypes.elementType.isRequired,
  /** @ignore */
  className: PropTypes.string,
}

export default Link
