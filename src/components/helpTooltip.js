import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from 'tss-react/mui'
import { Tooltip } from '@mui/material'
import HelpOutlineIcon from '@mui/icons-material/HelpOutline'

const useStyles = makeStyles({ name: 'helpTooltip' })({
  icon: {
    verticalAlign: 'middle',
    width: 16,
    height: 16,
    marginLeft: 2,
    position: 'relative',
    top: '-1px',
  },
  tooltip: {
    fontSize: '.9em',
  },
})

/**
 * Help Icon with integrated tooltip.
 *
 * @component
 */
function HelpTooltip({ title, ...props }) {
  const { classes } = useStyles()

  return (
    <Tooltip title={title} classes={{ tooltip: classes.tooltip }} {...props}>
      <HelpOutlineIcon className={classes.icon} fontSize='small' />
    </Tooltip>
  )
}

HelpTooltip.propTypes = {
  /** Content of the tooltip */
  title: PropTypes.node,
}

export default HelpTooltip
