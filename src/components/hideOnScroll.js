import React from 'react'
import PropTypes from 'prop-types'
import Slide from '@mui/material/Slide'
import { useScrollTrigger } from '@mui/material'

/**
 * Hides the child elements on scroll in the configured direction.
 *
 * @component
 */
function HideOnScroll({ enabled, direction, children }) {
  const trigger = useScrollTrigger()

  const triggered =
    (direction === 'down' && trigger) || (direction === 'up' && !trigger)

  return (
    <Slide appear={false} direction={direction} in={!enabled || !triggered}>
      {children}
    </Slide>
  )
}

HideOnScroll.defaultProps = {
  direction: 'down',
}

HideOnScroll.propTypes = {
  /** Specifies whether to hide the element on scroll. */
  enabled: PropTypes.bool,
  /** Specifies the scroll direction which triggers the hide effect. */
  direction: PropTypes.oneOf(['down', 'up']),
  /** Child react components */
  children: PropTypes.node.isRequired,
}

export default HideOnScroll
