import React, { useState, useRef, useCallback, useEffect } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from 'tss-react/mui'
import CloseIcon from '@mui/icons-material/Close'
import {
  TextField,
  Button,
  IconButton,
  Paper,
  Popper,
  MenuItem,
  Typography,
  MenuList,
} from '@mui/material'

const useStyles = makeStyles({ name: 'selectTextField' })(theme => ({
  root: {
    display: 'flex',
  },
  clearIndicator: {
    marginRight: -2,
    padding: 4,
    color: theme.palette.action.active,
    visibility: 'hidden',
  },
  clearIndicatorDirty: {
    visibility: 'visible',
  },
  option: {
    minHeight: 48,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    cursor: 'pointer',
    paddingTop: 6,
    boxSizing: 'border-box',
    outline: '0',
    WebkitTapHighlightColor: 'transparent',
    paddingBottom: 6,
    paddingLeft: 16,
    paddingRight: 16,
    [theme.breakpoints.up('sm')]: {
      minHeight: 'auto',
    },
    '&[aria-selected="true"]': {
      backgroundColor: theme.palette.action.selected,
    },
    '&[data-focus="true"]': {
      backgroundColor: theme.palette.action.hover,
    },
    '&[aria-disabled="true"]': {
      opacity: 0.5,
      pointerEvents: 'none',
    },
    '&:active': {
      backgroundColor: theme.palette.action.selected,
    },
  },
  textfield: {
    width: 'inherit',
  },
  input: {
    flex: 1,
  },
  button: {
    marginLeft: theme.spacing(1),
  },
  popper: {
    zIndex: theme.zIndex.modal,
  },
  menuList: {
    maxHeight: '50vh',
    overflowY: 'auto',
  },
}))

/**
 * Textfield behaving as a select field.
 *
 * The user can type some text in the textfield. The component will show
 * suitable options in a list below the field. The user must have selected
 * such an option so that the field gets valid.
 *
 * @component
 */
function SelectTextField({
  id: idProp,
  label,
  value,
  getOptionLabel,
  createEntryText,
  createEntryTextShort,
  disabled: disabledProp,
  options,
  onChange,
  onOpen,
  onClose,
  clearText,
  'aria-label': ariaLabel,
  className,
  ...props
}) {
  const [createNewEntry, setCreateNewEntry] = useState(false)
  const [inputValue, setInputValue] = useState(value)
  const [open, setOpen] = useState(false)
  const [anchorElement, setAnchorElement] = useState(null)
  const firstFocus = useRef(true)
  const inputRef = useRef(null)
  const { classes } = useStyles()

  const [defaultId, setDefaultId] = React.useState()
  const id = idProp || defaultId

  useEffect(() => {
    // Fallback to this default id when possible.
    // Use the random value for client-side rendering only.
    // We can't use it server-side.
    setDefaultId(`select-textfield-${Math.round(Math.random() * 1e5)}`)
  }, [])

  useEffect(() => {
    setInputValue(value)
  }, [value])

  const filteredOptions = useCallback(() => {
    if (!inputValue) {
      return options
    }
    const regex = RegExp(`.*(${inputValue}).*`, 'gi')
    return options.filter(item => regex.test(item))
  }, [inputValue])

  const handleOpen = () => {
    if (open) {
      return
    }

    if (onOpen) {
      onOpen()
    }
    setOpen(true)
  }

  const handleClose = () => {
    if (!open) {
      return
    }

    if (onClose) {
      onClose()
    }
    setOpen(false)
    setInputValue(value)
  }

  // Focus the input when first interacting with the combobox
  const handleRootClick = () => {
    if (
      firstFocus.current &&
      inputRef.current.selectionEnd - inputRef.current.selectionStart === 0
    ) {
      inputRef.current.focus()
    }

    firstFocus.current = false
  }

  const handleMouseDown = event => {
    if (event.target.getAttribute('id') !== id) {
      event.preventDefault()
    }
  }

  const handleItemSelected = option => {
    onChange({ target: { value: option } })
    setCreateNewEntry(false)
    handleClose()
  }

  const handleInputChange = event => {
    const newValue = event.target.value
    setInputValue(newValue)
    if (createNewEntry) {
      onChange({ target: { value: newValue || null } })
    }
    // automatically open list again if it was closed in the meantime
    handleOpen()
  }

  const handleClear = () => {
    setInputValue(null)
    onChange({ target: { value: null } })
  }

  const handleCreateNewEntryClicked = () => {
    setCreateNewEntry(true)
    onChange({ target: { value: inputValue || null } })
  }

  const disabled = disabledProp || !!(value && inputValue && !createNewEntry)
  const dirty = inputValue && inputValue.length > 0

  const renderListOption = (option, index) => {
    return (
      <MenuItem
        key={index}
        onClick={() => {
          handleItemSelected(option)
        }}
      >
        <Typography variant='inherit'>{getOptionLabel(option)}</Typography>
      </MenuItem>
    )
  }

  return (
    <div
      className={classes.root}
      onMouseDown={handleMouseDown}
      onClick={handleRootClick}
      {...props}
    >
      <TextField
        id={id}
        label={label}
        aria-label={ariaLabel}
        value={inputValue || ''}
        disabled={disabled}
        onFocus={handleOpen}
        onBlur={handleClose}
        onChange={handleInputChange}
        className={classes.textfield}
        InputProps={{
          ref: setAnchorElement,
          className: classes.inputRoot,
          // Disable browser's suggestion that might overlap with the popup.
          // Handle autocomplete but not autofill.
          autoComplete: 'nope',
          autoCapitalize: 'none',
          spellCheck: 'false',
          endAdornment: (
            <div className={classes.endAdornment}>
              {createNewEntry && (
                <Button disabled>{createEntryTextShort}</Button>
              )}
              <IconButton
                aria-label={clearText}
                title={clearText}
                className={[
                  classes.clearIndicator,
                  dirty ? classes.clearIndicatorDirty : null,
                ].join(' ')}
                onClick={handleClear}
              >
                <CloseIcon />
              </IconButton>
            </div>
          ),
        }}
        inputProps={{
          ref: inputRef,
          className: classes.input,
          disabled,
        }}
      />
      {open &&
      anchorElement &&
      (!createNewEntry || filteredOptions().length > 0) ? (
        <Popper
          className={classes.popper}
          style={{
            width: anchorElement ? anchorElement.clientWidth : null,
          }}
          role='presentation'
          anchorEl={anchorElement}
          open
        >
          <Paper className={classes.paper}>
            <MenuList className={classes.menuList}>
              {filteredOptions().map((option, index) =>
                renderListOption(option, index)
              )}
              {!createNewEntry && (
                <MenuItem onClick={handleCreateNewEntryClicked}>
                  <Typography variant='inherit'>{createEntryText}</Typography>
                </MenuItem>
              )}
            </MenuList>
          </Paper>
        </Popper>
      ) : null}
    </div>
  )
}

SelectTextField.defaultProps = {
  onChange: () => {},
  getOptionLabel: x => x,
  clearText: 'clear',
  createEntryText: 'Create a new entry',
  createEntryTextShort: 'New entry',
}

SelectTextField.propTypes = {
  /** @ignore */
  className: PropTypes.string,
  /** Used as the field id. Uses a randomly generated one if not set. */
  id: PropTypes.string,
  /** Label text for the text field. */
  label: PropTypes.string,
  /** If `true`, the input will be disabled. */
  disabled: PropTypes.bool,
  /** Array of options. */
  options: PropTypes.array.isRequired,
  /** Callback when input text has changed */
  onChange: PropTypes.func,
  /** Callback when the autocomplete list will be opened. */
  onOpen: PropTypes.func,
  /** Callback when the autocomplete list will be closed. */
  onClose: PropTypes.func,
  /** The value of the field. */
  value: PropTypes.string,
  /** Label used to improve accessibility. */
  'aria-label': PropTypes.string,
  /** Used to determine the string value for a given option. */
  getOptionLabel: PropTypes.func,
  /** Override the default text for the *clear* icon button. */
  clearText: PropTypes.string,
  /**
   * Override the default text for the *create entry* hint rendered
   * next to the input field.
   */
  createEntryText: PropTypes.string,
  /** Override the default text for the *create entry* menu item. */
  createEntryTextShort: PropTypes.string,
}

export default SelectTextField
