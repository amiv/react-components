import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from 'tss-react/mui'
import LaunchIcon from '@mui/icons-material/Launch'
import { Link } from '@mui/material'

const useStyles = makeStyles({ name: 'externalLink' })({
  icon: {
    verticalAlign: 'middle',
    width: 16,
    height: 16,
    marginLeft: 2,
  },
})

/**
 * Link component for external targets.
 *
 * @component
 */
function ExternalLink({ noIcon, children, ...props }) {
  const { classes } = useStyles()
  return (
    <Link {...props} target='_blank' rel='noopener noreferrer'>
      {children}
      {!noIcon && <LaunchIcon className={classes.icon} />}
    </Link>
  )
}

ExternalLink.propTypes = {
  /** Target URL */
  href: PropTypes.string.isRequired,
  /** Text color */
  color: PropTypes.string,
  /** Specifies that the icon should not be drawn. */
  noIcon: PropTypes.bool,
  /** Content of the link (text) */
  children: PropTypes.node.isRequired,
  /** Additional class applied to the outermost box. */
  className: PropTypes.string,
}

ExternalLink.defaultProps = {
  color: 'secondary',
  noIcon: false,
}

export default ExternalLink
